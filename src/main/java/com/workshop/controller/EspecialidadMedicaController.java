package com.workshop.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.workshop.models.EspecialidadMedica;
import com.workshop.service.IEspecialidadMedicaService;
@RestController
@RequestMapping("/especialidadesmedicas")
public class EspecialidadMedicaController {
	
	@Autowired
	IEspecialidadMedicaService service;
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<EspecialidadMedica>>listar(){
		List<EspecialidadMedica> especialistas=new ArrayList<>();
		especialistas=service.getAll();
		return new ResponseEntity<List<EspecialidadMedica>>(especialistas, HttpStatus.OK);

	}
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public EspecialidadMedica registrar(@RequestBody EspecialidadMedica v) {
		return service.persist(v);
	}


@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
public ResponseEntity<EspecialidadMedica> listarId(@PathVariable("id") Integer id){
	
	EspecialidadMedica especialista=new EspecialidadMedica();
	especialista=service.findById(id);
	return new ResponseEntity<EspecialidadMedica>(especialista,HttpStatus.OK);
}
}

	

	