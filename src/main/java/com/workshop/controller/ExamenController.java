package com.workshop.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.workshop.models.Examen;
import com.workshop.service.IExamenService;

@RestController
@RequestMapping("/examenes")
public class ExamenController {
	
	@Autowired
	IExamenService service;
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Examen>>listar(){
		List<Examen> examenes=new ArrayList<>();
		examenes=service.getAll();
		return new ResponseEntity<List<Examen>>(examenes, HttpStatus.OK);

	}
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Examen registrar(@RequestBody Examen v) {
		return service.persist(v);
	}


@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
public ResponseEntity<Examen> listarId(@PathVariable("id") Integer id){
	
	Examen examen=new Examen();
	examen=service.findById(id);
	return new ResponseEntity<Examen>(examen,HttpStatus.OK);
}
}
