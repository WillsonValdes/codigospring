package com.workshop.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.workshop.models.Examen;
import com.workshop.models.Medico;
import com.workshop.service.IExamenService;
import com.workshop.service.IMedicoService;

@RestController
@RequestMapping("/medicos")
public class MedicoController {
	
	@Autowired
	IMedicoService service;
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Medico>>listar(){
		List<Medico> medicos=new ArrayList<>();
		medicos=service.getAll();
		return new ResponseEntity<List<Medico>>(medicos, HttpStatus.OK);

	}
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Medico registrar(@RequestBody Medico v) {
		return service.persist(v);
	}


@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
public ResponseEntity<Medico> listarId(@PathVariable("id") Integer id){
	
	Medico medico=new Medico();
	medico=service.findById(id);
	return new ResponseEntity<Medico>(medico,HttpStatus.OK);
}
}
