package com.workshop.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.workshop.models.Examen;
import com.workshop.models.Paciente;
import com.workshop.service.IExamenService;
import com.workshop.service.IPacienteService;

@RestController
@RequestMapping("/pacientes")
public class PacienteController {

	@Autowired
	IPacienteService service;
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Paciente>>listar(){
		List<Paciente> pacientes=new ArrayList<>();
		pacientes=service.getAll();
		return new ResponseEntity<List<Paciente>>(pacientes, HttpStatus.OK);

	}
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public Paciente registrar(@RequestBody Paciente v) {
		return service.persist(v);
	}


@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
public ResponseEntity<Paciente> listarId(@PathVariable("id") Integer id){
	
	Paciente paciente=new Paciente();
	paciente=service.findById(id);
	return new ResponseEntity<Paciente>(paciente,HttpStatus.OK);
}
	
}
