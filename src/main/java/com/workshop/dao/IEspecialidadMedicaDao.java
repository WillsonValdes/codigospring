package com.workshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.workshop.models.EspecialidadMedica;
@Repository
public interface IEspecialidadMedicaDao extends JpaRepository <EspecialidadMedica, Integer> {

}
