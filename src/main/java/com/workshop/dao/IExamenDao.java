package com.workshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.workshop.models.Examen;

@Repository
public interface IExamenDao extends JpaRepository<Examen, Integer> {

}
