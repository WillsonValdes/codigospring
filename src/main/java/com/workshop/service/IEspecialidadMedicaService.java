package com.workshop.service;

import java.util.List;

import com.workshop.models.EspecialidadMedica;

public interface IEspecialidadMedicaService {
	EspecialidadMedica persist(EspecialidadMedica e);
	List<EspecialidadMedica> getAll();
	EspecialidadMedica findById(Integer id);
	EspecialidadMedica merge(EspecialidadMedica e);
	void delete(Integer id);
	
	
}
