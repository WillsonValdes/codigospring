package com.workshop.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.workshop.dao.IEspecialidadMedicaDao;
import com.workshop.models.EspecialidadMedica;
import com.workshop.service.IEspecialidadMedicaService;
@Service
public class EspecialidadMedicaService implements IEspecialidadMedicaService {

	@Autowired
	IEspecialidadMedicaDao service;
	
	@Override
	public EspecialidadMedica persist(EspecialidadMedica e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public List<EspecialidadMedica> getAll() {
		// TODO Auto-generated method stub
		return service.findAll();
	}

	@Override
	public EspecialidadMedica findById(Integer id) {
		// TODO Auto-generated method stub
		return service.findOne(id);
	}

	@Override
	public EspecialidadMedica merge(EspecialidadMedica e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		service.delete(id);
		
	}

}
