package com.workshop.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.workshop.dao.IExamenDao;
import com.workshop.models.Examen;
import com.workshop.service.IExamenService;

@Service
public class ExamenService implements IExamenService {

	@Autowired
	IExamenDao service;
	@Override
	public Examen persist(Examen e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public List<Examen> getAll() {
		// TODO Auto-generated method stub
		return service.findAll();
	}

	@Override
	public Examen findById(Integer id) {
		// TODO Auto-generated method stub
		return service.findOne(id);
	}

	@Override
	public Examen merge(Examen e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		
	}

}
